"""An AWS Python Pulumi program"""

import pulumi
import pulumi_awsx as awsx
import pulumi_eks as eks
from components import EksService

config = pulumi.Config()
min_cluster_size = config.get_float("minClusterSize", 3)
max_cluster_size = config.get_float("maxClusterSize", 6)
desired_cluster_size = config.get_float("desiredClusterSize", 3)
eks_node_instance_type = config.get("eksNodeInstanceType", "t2.medium")
service_message = config.get("message", "abc123")
# Create ECR repository
ecr_repository = awsx.ecr.Repository(
    "pulumi-challenge-repository",
    name="pulumi-challenge-repository",
)
pulumi.export("ecr_registry", ecr_repository.url)

# Build docker image and push to repository
my_app_image = awsx.ecr.Image(
    "my-app-image", repository_url=ecr_repository.url, path=".",
    env={
        "MESSAGE": service_message,
    }
)

# Create VPC
vpc = awsx.ec2.Vpc(
    "pulumi-challenge-vpc",
    args=awsx.ec2.VpcArgs(),
    opts=pulumi.ResourceOptions(),
)

# Export the VPC ID
pulumi.export("vpc_id", vpc.id)

# Create the EKS cluster
eks_cluster = eks.Cluster(
    "eks-cluster",
    # Put the cluster in the new VPC created earlier
    vpc_id=vpc.vpc_id,
    # Public subnets will be used for load balancers
    public_subnet_ids=vpc.public_subnet_ids,
    # Private subnets will be used for cluster nodes
    private_subnet_ids=vpc.private_subnet_ids,
    # Change configuration values to change any of the following settings
    instance_type=eks_node_instance_type,
    desired_capacity=desired_cluster_size,
    min_size=min_cluster_size,
    max_size=max_cluster_size,
    # Do not give worker nodes a public IP address
    node_associate_public_ip_address=False,
    # Uncomment the next two lines for private cluster (VPN access required)
    # endpoint_private_access=true,
    # endpoint_public_access=false
)

# initialize service
EksService.EksService(
    "my-eks-service",
    kubeconfig=eks_cluster.kubeconfig,
    image_uri=my_app_image.image_uri,
)
