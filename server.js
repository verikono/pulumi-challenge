'use strict';

const express = require('express');

// Constants
const PORT = 8080;
const HOST = '127.0.0.1';

const MESSAGE = `${process.env.MESSAGE}`
// App
const app = express();
const response = 'Hello World ' + MESSAGE
app.get('/', (req, res) => {
  res.send(response);
});

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);
console.log(`Response is "${response}"`)
